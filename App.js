import React from 'react';
import { StyleSheet, WebView } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <WebView
              source={{uri: 'https://blackridgesoftware.com/'}}
              style={styles.container}
            />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});
